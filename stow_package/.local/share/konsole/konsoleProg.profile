[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=martinxColorScheme
DimmValue=51
Font=FiraCode Nerd Font Mono,11,-1,5,50,0,0,0,0,0

[Cursor Options]
CursorShape=0
CustomCursorColor=255,255,255
CustomCursorTextColor=0,0,0
UseCustomCursorColor=false

[General]
DimWhenInactive=false
Name=konsoleProg
Parent=FALLBACK/
TerminalColumns=102
TerminalRows=32

[Scrolling]
HistorySize=2000

[Terminal Features]
BlinkingCursorEnabled=false
VerticalLine=false
