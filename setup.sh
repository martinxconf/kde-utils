#!/bin/bash
# setup.sh
# kde-utils
#
# Applies a stow package with config files to $HOME

function status() {
	echo "=== kde-utils setup ===" "$@"
}

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
status "Detected script location: ${SCRIPT_DIR}"
status "Applying stow package..."

stow -v stow_package/ --no-folding --dir="${SCRIPT_DIR}" --target="${HOME}"

if (( $? != 0 )); then
	status "ERROR: stow failed"
	exit 1
else
	status "OK: kde-utils configured"
fi


status "COMPILING YAKUAKE"
status "Installing yakuake compile deps..."
sudo apt install extra-cmake-modules qtdeclarative5-dev qtbase5-dev \
        libqt5svg5-dev \
        libkf5archive-dev libkf5config-dev libkf5coreaddons-dev \
        libkf5crash-dev libkf5dbusaddons-dev libkf5globalaccel-dev \
        libkf5i18n-dev libkf5iconthemes-dev libkf5kio-dev \
        libkf5newstuff-dev libkf5notifications-dev libkf5notifyconfig-dev \
        libkf5parts-dev libkf5widgetsaddons-dev libkf5windowsystem-dev \
        libqt5x11extras5-dev

YAKUAKE_CLONE_DIR=${HOME}/local/compile/tool/yakuake
mkdir -p "${YAKUAKE_CLONE_DIR}"
if [ ! -d "${YAKUAKE_CLONE_DIR}/.git" ]; then
        git clone https://github.com/KDE/yakuake.git "${YAKUAKE_CLONE_DIR}"
fi
cd ${YAKUAKE_CLONE_DIR}
mkdir build
cd build
#cmake ../. -DCMAKE_INSTALL_PREFIX=${HOME}/local/install/tool
cmake ../. -DCMAKE_INSTALL_PREFIX=/usr/local
make -j
sudo make install

which yakuake
if (( $? != 0 )); then
        status "ERROR: couldn't find yakuake after make install"
        exit 1
fi

status "OK: yakuake compiled"
