# KDE additional setup

# Contents

## .config/kglobalshortcutsrc
Notably sets yakuake "open" shortcut to "superscript 2" (key above Tab key on AZERTY).
Dolphin misc config.

## .config/yakuakerc
yakuake shortcuts

## .config/dolphinrc
Dolphin file browser.

## .config/okularpartrc
Okular PDF viewer.

## .local/share/konsole/konsoleProg.profile
## .local/share/konsole/martinxColorScheme.colorscheme

konsole: fonts, appearance, initial terminal size (used by yakuake).
